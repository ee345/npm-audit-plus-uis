# NPM Audit+ UIS

Forked from https://github.com/freedomofpress/npm-audit-plus

**NPM Audit+ UIS** is a wrapper around NPM's [built-in audit tool](https://docs.npmjs.com/cli/audit). It adds the following functionality:

- Ignore particular advisories
- Specify a project to audit
- Output audit result as JUnit XML, compatible with many CI systems

## Installation

Install globally:

```sh
npm install -g npm-audit-plus-uis
```

or install locally:

```sh
npm install npm-audit-plus-uis
```

Either works!

## Usage

```sh
npm-audit-plus-uis --ignore=123,456 --xml             # If installed globally
$(npm bin)/npm-audit-plus-uis --ignore=123,456 --xml  # If installed locally
```

For more documentation on NPM Audit+'s flags, run

```sh
npm-audit-plus-uis --help             # If installed globally
$(npm bin)/npm-audit-plus-uis --help  # If installed locally
```

Other usage

```sh
npx npm-audit-plus-uis --production --xml # Ignore vulnerabilities in devDependencies
```

```sh
npx npm-audit-plus-uis --auditLevel critical --xml # Only exit with code non-zero on high and critical
```

## Gitlab example

```yml
audit-npm:
  script:
    - npm ci
    - npx npm-audit-plus-uis --xml > npm-audit.junit.xml
  allow_failure: true
  artifacts:
    when: always
    paths:
      - '*.junit.xml'
    reports:
      junit: npm-audit.junit.xml
    expire_in: 1 day
```
